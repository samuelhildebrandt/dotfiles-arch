#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

BROWSER=/opt/google/chrome/chrome
EDITOR=/usr/bin/vim

set -o vi

if [[ -z "$TMUX" ]]; then
    tmux attach-session -t default || tmux new-session -s default -c /home/sh/workspace
fi
