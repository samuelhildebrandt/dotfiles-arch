function jira --description 'Gets Jira number from Git branch name'
    git rev-parse --abbrev-ref HEAD | awk -F'[/-]' '{ print $2"-"$3 }'
end
