function br --description 'Prints name of Git branch'
    printf (git rev-parse --abbrev-ref HEAD)
end
