# Default shell
set-option -g default-shell /usr/bin/fish

# Status bar
set-option -g status-position top
set-option -g status-interval 2
set-option -g status-right ""
set-option -g status-left " "
set-option -g status-bg default
set-window-option -g window-status-format '#I:#W#F'
set-window-option -g window-status-current-format '#I:#W#F'
set-window-option -g window-status-style fg="colour7",bg=default
set-window-option -g window-status-current-style fg=colour0,bg=colour3

# Message style
set-option -g message-style fg=colour0,bg=colour3

# Border style
set-option -g pane-border-style fg=colour8
set-option -g pane-active-border-style fg=colour7

# Automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on

# Larger history
set-option -g history-limit 10000

# Misc
set-window-option -g xterm-keys on                          # Allows using ctrl-key to jump words in prompt
set-window-option -g pane-base-index 1                      # Start pane index from 1
set-option -g base-index 1                                  # Start window index from 1
set-option -sg escape-time 0                                # No delay for escape key press

# Key bindings

# Vi keys
set-option -g status-keys vi
set-window-option -g mode-keys vi

# Prefix key
set-option -g prefix C-Space
unbind C-Space
bind C-Space send-prefix

# Manage panes and windows
bind-key ! break-pane                                       # Break pane into new window
bind-key @ choose-window 'join-pane -s "%%"'                # Join pane to current window
bind-key '#' command-prompt 'rename-window %%'              # Rename window
bind-key '$' command-prompt -I "#S" "rename-session '%%'"   # Rename session

# Open panes with current path
bind-key % split-window -h -c "#{pane_current_path}"
bind-key ^ split-window -c "#{pane_current_path}"

# Vi keys

# Use Alt to switch panes
bind -n M-h select-pane -L
bind -n M-j select-pane -D 
bind -n M-k select-pane -U
bind -n M-l select-pane -R

# Use Alt + Shift to switch windows
bind-key -n M-H previous-window
bind-key -n M-H next-window

# Use Ctrl + Alt to swap panes
bind -n C-M-j swap-pane -D
bind -n C-M-k swap-pane -U

# Use Ctrl + Alt to swap window
bind-key -n C-M-h swap-window -t -1\; select-window -t -1
bind-key -n C-M-l swap-window -t +1\; select-window -t +1

# Resize panes
bind-key h command-prompt 'resize-pane -L "%%"'
bind-key j command-prompt 'resize-pane -D "%%"'
bind-key k command-prompt 'resize-pane -U "%%"'
bind-key l command-prompt 'resize-pane -R "%%"'

# Arrow keys

# Use Alt to switch panes
bind -n M-Left select-pane -L
bind -n M-Down select-pane -D 
bind -n M-Up select-pane -U
bind -n M-Right select-pane -R

# Use Shift to switch windows
bind-key -n S-Left previous-window
bind-key -n S-Right next-window

# Use Alt + Shift to swap panes 
bind -n M-S-Down swap-pane -D
bind -n M-S-Up swap-pane -U

# Use Alt + Shift to swap window
bind-key -n M-S-Left swap-window -t -1\; select-window -t -1
bind-key -n M-S-Right swap-window -t +1\; select-window -t +1

# Resize panes
bind-key Left command-prompt 'resize-pane -L "%%"'
bind-key Down command-prompt 'resize-pane -D "%%"'
bind-key Up command-prompt 'resize-pane -U "%%"'
bind-key Right command-prompt 'resize-pane -R "%%"'

# Reload config file
unbind-key R
bind-key R source-file ~/.tmux.conf \; display-message "Config reloaded..."
