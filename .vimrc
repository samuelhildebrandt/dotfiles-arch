if !has('gui_running')
    set t_Co=256
endif

if &t_Co > 2 || has("gui_running")
    syntax on
endif

set updatetime=100                  " Delay for updates
set hidden                          " Hides buffers instead of closing them
set splitbelow                      " Open horizontal splits below
set splitright                      " Open vertical splits to the right
set backspace=indent,eol,start      " allow backspacing over everything in insert mode
set history=1000                    " remember more commands and search history
set undolevels=1000                 " use many levels of undo
set visualbell                      " don't beep
set noerrorbells                    " don't beep
set nobackup                        " don't write backup files
set noswapfile                      " don't write swap files
set showmatch                       " set show matching parenthesis

" Search
set ignorecase                      " ignore case when searching
set smartcase                       " ignore case if search pattern is all lowercase,
set hlsearch                        " highlight search terms
set incsearch                       " show search matches as you type

" Lines numbers
set number
set relativenumber

" Highlight for line number
highlight LineNr ctermfg=darkgrey
set cursorline
highlight clear CursorLine
highlight clear CursorLineNR
augroup CLClear
    autocmd! ColorScheme * hi clear CursorLine
augroup END
hi CursorLineNR ctermfg=0 ctermbg=3
augroup CLNRSet
    autocmd! ColorScheme * hi CursorLineNR ctermfg=0 ctermbg=3
augroup END

" Indentation
filetype plugin indent on           " show existing tab with 4 spaces width
set autoindent                      " always set autoindenting on
set copyindent                      " copy the previous indentation on autoindenting
set expandtab                       " insert spaces when tab is pressed
set tabstop=4                       " a tab is four spaces
set shiftwidth=4                    " number of spaces to use for autoindenting
set shiftround                      " use multiple of shiftwidth when indenting with '<' and '>'
set nowrap                          " don't wrap lines

" Key bindings
set pastetoggle=<F2>
let mapleader = "\<Space>" 

" map <up> <nop>
" map <down> <nop>
" map <left> <nop>
" map <right> <nop>

nnoremap ; :
cmap w!! w !sudo tee % >/dev/null

nnoremap <silent> <leader>/ :nohlsearch<CR>
nnoremap <silent> <leader>ce :e $MYVIMRC<CR>
nnoremap <silent> <leader>cr :so $MYVIMRC<CR>

nnoremap <leader>s :update<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>z :wq!<CR>
nnoremap <leader>l :<Up><CR>
nnoremap <leader>t :NERDTreeToggle<CR>

" Navigate tabs and windows
nnoremap <leader>H :tabprevious<CR>
nnoremap <leader>L :tabnext<CR>
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l

" fzf
nnoremap <leader>o :Files<CR>
nnoremap <leader>O :GFiles<CR>
nnoremap <leader>r :History:<CR>
nnoremap <leader>R :History/<CR>
nnoremap <leader>f :BLines<CR>
nnoremap <leader>F :Lines<CR>
nnoremap <leader>b :Buffers<CR>
nnoremap <leader>w :Windows<CR>

" Install and run vim-plug on first run
if empty(glob('~/.vim/autoload/plug.vim'))
    set silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
 endif

so ~/.vim/plugins.vim

highlight GitGutterAdd ctermfg=green ctermbg=NONE
highlight GitGutterChange ctermfg=yellow ctermbg=NONE
highlight GitGutterDelete ctermfg=red ctermbg=NONE
highlight GitGutterChangeDelete ctermfg=yellow ctermbg=NONE

let NERDTreeShowHidden = 1
